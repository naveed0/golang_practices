// PERFORMING CRUD with (GO-LANG/GORM/GIN-FRAMEWORK/POSTSGRES-DATABASE)

package main

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/lib/pq"
)

// Creating a variable of GORM.DB that will be used every time to interact
// with database.
var db *gorm.DB
var err error

// Person is a model.
type Person struct {
	//Parsing JSON into the struct
	ID        uint   `json:"id"`
	FirstName string `json:"firstname"`
	LastName  string `json:"lastname"`
}

func main() {

	// Establishing the connection with database (Postgres)
	db, err = gorm.Open("postgres", "user=peoples_user password=secret dbname=peoples_db sslmode=disable")
	if err != nil {
		fmt.Println(err)
	}
	// Close the resource of db with the keyword `defer` .
	defer db.Close()

	db.AutoMigrate(&Person{})

	// Gin framework ( r --> router )
	r := gin.Default()
	// URLS
	r.GET("/", GetPeople)
	r.GET("/people/:id/:fname", GetPerson)
	r.POST("/people/:fname/:lname", CreatePerson)
	r.PUT("/people/:id", UpdatePerson)
	r.DELETE("/people/:id", DeletePerson)
	r.Run(":8082")
}

//DeletePerson is (delete the record in the database against the id provided).
func DeletePerson(c *gin.Context) {
	// (c *gin.Context) mentioned in above function is the pointer of Gin Framework.
	id := c.Params.ByName("id")
	var person Person
	d := db.Where("id = ?", id).Delete(&person)
	fmt.Println(d)
	c.JSON(200, gin.H{"id #" + id: "deleted"})
}

//UpdatePerson is (updates the record in the database against the id provided).
func UpdatePerson(c *gin.Context) {
	var person Person
	id := c.Params.ByName("id")
	if err := db.Where("id = ?", id).First(&person).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	}
	//binding the data in JSON and saving in DB.
	c.BindJSON(&person)
	db.Save(&person)
	c.JSON(200, person)
}

//CreatePerson is (create a new record in the database).
func CreatePerson(c *gin.Context) {
	var person Person
	person.FirstName = c.Params.ByName("fname")
	person.LastName = c.Params.ByName("lname")

	c.BindJSON(&person)
	db.Create(&person)
	c.JSON(200, person)
}

//GetPerson is (get one specific record from the database against the id provided).
func GetPerson(c *gin.Context) {
	id := c.Params.ByName("id")
	fname := c.Params.ByName("fname")
	var person Person
	if err := db.Where("id = ? OR first_name=?", id, fname).Find(&person).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		c.JSON(200, person)
	}
}

//GetPeople is (get all the records from the database).
func GetPeople(c *gin.Context) {
	var people []Person
	if err := db.Find(&people).Error; err != nil {
		c.AbortWithStatus(404)
		fmt.Println(err)
	} else {
		c.JSON(200, people)
	}
}
